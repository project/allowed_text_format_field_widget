<?php

namespace Drupal\allowed_text_format_field_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\text\Plugin\Field\FieldWidget\TextareaWidget;

/**
 * Defines the 'allowed_text_format_field_widget' field widget.
 *
 * @FieldWidget(
 *   id = "allowed_text_format_field_widget",
 *   label = @Translation("Allowed Text Format"),
 *   field_types = {
 *     "text_long"
 *   }
 * )
 */
class AllowedTextFormatFieldWidget extends TextareaWidget {

  /**
   * Format field name.
   *
   * @const string
   */
  const FIELD_FORMAT = 'allowed_format';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      static::FIELD_FORMAT => static::getAllAvailableTextFormats(),
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $summary[static::FIELD_FORMAT] = $this->t('Allowed formats : @formats',
                                              ['@formats' => implode(', ', $this->getAllowedTextFormats())]);

    return $summary;
  }

  /**
   * Return the list of available text formats labels.
   *
   * @return string[]
   *   The list of all available text formats.
   */
  public static function getAllAvailableTextFormats() {
    $editors = \Drupal::entityTypeManager()
      ->getStorage('filter_format')
      ->loadMultiple();

    return array_map(function ($editor) {
      return $editor->label();
    }, $editors);
  }

  /**
   * Return the list of selected text format labels.
   *
   * @return string[]
   *   List of allowed text formats labels.
   */
  public function getAllowedTextFormats() {
    $selection = array_filter($this->getSetting(static::FIELD_FORMAT));
    if (empty($selection)) {
      return static::getAllAvailableTextFormats();
    }

    return array_intersect_key(static::getAllAvailableTextFormats(), $selection);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $element = parent::settingsForm($form, $form_state);

    // Add format selector.
    $element[static::FIELD_FORMAT] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Available formats'),
      '#default_value' => $this->getSetting(static::FIELD_FORMAT),
      '#options' => static::getAllAvailableTextFormats(),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element['#allowed_formats'] = array_keys($this->getAllowedTextFormats());

    return $element;
  }

}
