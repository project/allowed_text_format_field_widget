CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

 Provides a field widget "Allowed Text Format" that allows to restrict the list of
 editor formats for a formatted text field.

 The difference with a similar module "Allowed Text Format" is the availability
 to choose different restrictions according to form display type for a field.
 The other module "Allowed Text Format" provide restriction on field. So you can not
 define different text format restrictions according to form display types.
 That's why we created this module that provide more flexibility and reusability.


 * For a full description of the module, visit the project page:
   https://drupal.org/project/allowed_text_format_field_widget

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/allowed_text_format_field_widget

RECOMMENDED MODULES
-------------------

 * No extra module is required.

INSTALLATION
------------

 * Install as usual, see
   https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8 for further
   information.

CONFIGURATION
-------------

 * No configuration is needed.


MAINTAINERS
-----------

Current maintainers:

 * Thomas Sécher (tsecher) (https://www.drupal.org/u/tsecher)
 * Roxane Merlen (RoxaneMrln) (https://www.drupal.org/u/roxanemrln)
